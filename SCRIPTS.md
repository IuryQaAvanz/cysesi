# Scripts
Abaixo seguem algumas informações para execução dos testes e aramazenamento de evidencias.

## Vamos começar?

Os scripts foram definidos de forma que possam ser executados em modo headless onde eles serão executados sem uma interface gráfica aparente.

## Instruções

> **Obs 1:** Você precisará estar com o terminal aberto na raíz do projeto para executar os scripts.

## Gerando base de imagens

Para executar os testes de **regressão visual** gerando as imagens base que serão comparadas posteriormente, você precisará digitar ou copiar e colar no terminal o seguinte comando: `npm run geraimgsbase`, esse comando irá acessar e executar os testes definidos no arquivo _spec.cy.js_ dentro da pasta _cypress/e2e_, gerando imagens que servirão de base para o comando de comparação.

> **Obs 2:** Ao executar o comando acima, as imagens criadas ficarão salvas em uma pasta chamada _snapshots/layoutAprovado_.

## Comparando imagens

Para executar os teste de **regressão visual** comparando as imagens atuais com as imagens geradas anteriormente, você precisará digitar ou copiar e colar no terminal o seguinte comando  `npm run validalayout`, esse comando irá acessar e executar os testes definidos no arquivo _spec.cy.js_ dentro da pasta _cypress/e2e_, comparando as imagens que ele coletará atualmente com as imagens geradas anterioemente. 

Para ter explicações detalhadas sobre o código apresentado no projeto, clique [AQUI](./TESTES.md).