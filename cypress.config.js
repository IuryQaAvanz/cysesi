const os = require("os");
const path = require("path");

const { defineConfig } = require("cypress");
const { configureVisualRegression } = require("cypress-visual-regression");

module.exports = defineConfig({
  env: {
    visualRegressionType: "regression",
    failSilently: false,
    visualRegressionBaseDirectory: "cypress/snapshots/layoutAprovado",
    visualRegressionDiffDirectory: "cypress/snapshots/layoutAtual",
    visualRegressionGenerateDiff: false,
    CYPRESSVR_ERROR_THRESHOLD: 0.3,
    cypressVisualRegressionConfig: {
      baseDirectory: "cypress/snapshots/layoutAprovado",
      diffDirectory: "cypress/snapshots/layoutAtual",
    },
    screenshotsFolder: path.join(os.tmpdir(), "cypress", "screenshots"),
  },
  reporter: "cypress-multi-reporters",
  reporterOptions: {
    reporterEnabled: "cypress-mochawesome-reporter, mocha-junit-reporter",
    mochaJunitReporterReporterOptions: {
      mochaFile: "cypress/reports/junit/results-[hash].xml",
    },
    cypressMochawesomeReporterReporterOptions: {
      charts: true,
      reportPageTitle: "Relatório de testes",
      embeddedScreenshots: true,
      inlineAssets: true,
      saveAllAttempts: false,
    },
  },
  viewportWidth: 1280,
  viewportHeight: 720,
  pageLoadTimeout: 120000,
  retries: 2,
  trashAssetsBeforeRuns: true,
  video: true,
  e2e: {
    setupNodeEvents(on, config) {
      require("cypress-mochawesome-reporter/plugin")(on);
      configureVisualRegression(on, config);
      on("before:browser:launch", (browser = {}, launchOptions) => {
        if (browser.family === "chromium" && browser.name !== "electron") {
          launchOptions.args.push("--incognito");
          return launchOptions;
        }
      });
    },
  },
});
