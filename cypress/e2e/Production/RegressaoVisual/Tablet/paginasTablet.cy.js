const env = require("../../../../fixtures/env/env.prod.json");
const urls = require("../../../../fixtures/urls/urls.json");

describe(`Suite de testes (TABLET - PRODUCAO) para ${Cypress.env("action")} imagens`, () => {
  beforeEach(() => {
    cy.limparEstadoCypress();
  });

  it(`Acessa a página HOME no tablet e ${Cypress.env("motivoteste")} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["HOME PROD"]}`;
    cy.intercept(homeUrl).as("home");
    cy.visit(homeUrl);
    cy.wait(`@home`);

    cy.removeElementosHome();

    cy.compareSnapshot(`page_HOME`, {
      blackout: ["#adopt-controller-button"],
      blackout: [".adopt-c-blcsFr"],
      capture: "fullPage",
      errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  for (const [page, url] of Object.entries(urls)) {
    if (page === "HOME DEV" || page === "HOME PROD") {
      continue;
    }

    it(`Acessa a página ${page} no tablet e ${Cypress.env("motivoteste")} imagens para validar layout`, () => {
      const fullUrl = `${env.baseUrl}${url}`;
      cy.intercept("GET", fullUrl).as(page);
      cy.visit(fullUrl);
      cy.wait(`@${page}`);
      cy.removeElementos();

      cy.compareSnapshot(`page_${page}`, {
        blackout: ["#adopt-controller-button"],
        blackout: [".adopt-c-blcsFr"],
        capture: "fullPage",
        errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
      });
    });
  }
});
