const env = require("../../../fixtures/env.dev.json")
const urls = require("../../../fixtures/urls.json")

describe(`Suite de testes para ${Cypress.env('action')} imagens`, () => {

  var page =  'sobre-o-sesi';
  var url =  `${env.baseUrl}${page}`;
  var components =  [
    {
      'name': 'bloco01_banner_slide',
      'id': '13cbe087-54ef-46ab-bbf1-0f73a1d8d7cd',
    }
  ];
  it(`Teste de layout - página [${page}]`, () => {
    cy.intercept('GET', url).as(page);
    cy.visit(url);
    cy.wait(`@${page}`);
    for(let i in components){
      cy.get(`[id-component="${components[i].id}"]`).compareSnapshot(`Page_${page}_${components[i].name}`, 0.3);
    }
  });

  //testando

});
