const env = require("../../../../fixtures/env/env.dev.json");
const urls = require("../../../../fixtures/urls/devUrls.json");

describe(`Suite de testes (DESKTOP - Desenvolvimento) para ${Cypress.env(
  "action"
)} imagens`, () => {
  beforeEach(() => {
    cy.limparEstadoCypress();
  });

  it(`Acessa a página HOME e ${Cypress.env(
    "motivoteste"
  )} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["HOME DEV"]}`;
    cy.intercept("GET", homeUrl).as("home");
    cy.visit(homeUrl);
    cy.wait(`@home`);

    cy.removeElementosDevHome();
    cy.get(".vue-go-top").invoke("remove");

    cy.compareSnapshot(`page_HOME`, {
      errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  it(`Acessa a página CONSULTAS PRESENCIAIS e ${Cypress.env(
    "motivoteste"
  )} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["CONSULTAS PRESENCIAIS"]}`;
    cy.intercept("GET", homeUrl).as("CONSULTAS PRESENCIAIS");
    cy.visit(homeUrl);
    cy.wait(`@CONSULTAS PRESENCIAIS`);
    cy.reload();

    cy.removeElementosDevHome();

    cy.CardsEFiltrosEspecialidades();

    cy.compareSnapshot(`page_CONSULTAS PRESENCIAIS`, {
      errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  it(`Acessa a página EXAMES e ${Cypress.env(
    "motivoteste"
  )} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["EXAMES"]}`;
    cy.intercept("GET", homeUrl).as("EXAMES");
    cy.visit(homeUrl);
    cy.wait(`@EXAMES`);

    cy.removeElementosDevHome();
    cy.CardsEFiltrosEspecialidades();

    cy.compareSnapshot(`page_EXAMES`, {
      errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  it(`Acessa a página CONSULTAS ONLINE e ${Cypress.env(
    "motivoteste"
  )} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["CONSULTAS ONLINE"]}`;
    cy.intercept("GET", homeUrl).as("CONSULTAS ONLINE");
    cy.visit(homeUrl);
    cy.wait(`@CONSULTAS ONLINE`);

    cy.removeElementosDevHome();
    cy.CardsEFiltrosEspecialidades();

    cy.compareSnapshot(`page_CONSULTAS ONLINE`, {
      errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  it(`Acessa a página VACINAS e ${Cypress.env(
    "motivoteste"
  )} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["VACINAS"]}`;
    cy.intercept("GET", homeUrl).as("VACINAS");
    cy.visit(homeUrl);
    cy.wait(`@VACINAS`);

    cy.removeElementosDevHome();
    cy.CardsEFiltrosEspecialidades();

    cy.compareSnapshot(`page_VACINAS`, {
      errorThreshold:  Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  for (const [page, url] of Object.entries(urls)) {
    if (
      page === "HOME DEV" ||
      page === "CONSULTAS PRESENCIAIS" ||
      page === "EXAMES" ||
      page === "CONSULTAS ONLINE" ||
      page === "VACINAS"
    ) {
      continue;
    }

    it(`Acessa a página ${page} e ${Cypress.env(
      "motivoteste"
    )} imagens para validar layout`, () => {
      const fullUrl = `${env.baseUrl}${url}`;
      cy.intercept("GET", fullUrl).as(page);
      cy.visit(fullUrl);
      cy.wait(`@${page}`);

      cy.removeElementosDevDesktop();

      cy.compareSnapshot(`page_${page}`, {
        capture: "fullPage",
        errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
      });
    });
  }
});
