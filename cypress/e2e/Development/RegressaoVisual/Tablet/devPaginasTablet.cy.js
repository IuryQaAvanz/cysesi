const env = require("../../../../fixtures/env/env.dev.json");
const urls = require("../../../../fixtures/urls/devUrls.json");
//ajustando tablet
describe(`Suite de testes (TABLET - Desenvolvimento) para ${Cypress.env(
  "action"
)} imagens`, () => {
  beforeEach(() => {
    cy.limparEstadoCypress();
  });

  it(`Acessa a página HOME no tablet e ${Cypress.env(
    "motivoteste"
  )} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["HOME DEV"]}`;
    cy.intercept(homeUrl).as("home");
    cy.visit(homeUrl);
    cy.wait(`@home`);

    cy.removeElementosDev();
    cy.compareSnapshot(`page_HOME`, {
      capture: "fullPage",
      errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  it(`Acessa a página EXAMES no tablet e ${Cypress.env(
      "motivoteste"
  )} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["EXAMES"]}`;
    cy.intercept("GET", homeUrl).as("EXAMES");
    cy.visit(homeUrl);
    cy.wait(`@EXAMES`);

    cy.removeElementosDevHome();
    cy.CardsEFiltrosEspecialidades();

    cy.compareSnapshot(`page_EXAMES`, {
      errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  it(`Acessa a página RESULTADOS DE EXAMES no tablet e ${Cypress.env(
      "motivoteste"
  )} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["RESULTADOS DE EXAMES"]}`;
    cy.intercept("GET", homeUrl).as("RESULTADOS DE EXAMES");
    cy.visit(homeUrl);
    cy.wait(`@RESULTADOS DE EXAMES`);

    cy.removeElementosDevHome();

    cy.compareSnapshot(`page_RESULTADOS DE EXAMES`, {
      errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  it(`Acessa a página CONSULTAS PRESENCIAIS no tablet e ${Cypress.env(
      "motivoteste"
  )} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["CONSULTAS PRESENCIAIS"]}`;
    cy.intercept("GET", homeUrl).as("CONSULTAS PRESENCIAIS");
    cy.visit(homeUrl);
    cy.wait(`@CONSULTAS PRESENCIAIS`);
    cy.reload();

    cy.removeElementosDevHome();
    cy.CardsEFiltrosEspecialidades();

    cy.compareSnapshot(`page_CONSULTAS PRESENCIAIS`, {
      errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  it(`Acessa a página CONSULTAS ONLINE no tablet e ${Cypress.env(
      "motivoteste"
  )} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["CONSULTAS ONLINE"]}`;
    cy.intercept("GET", homeUrl).as("CONSULTAS ONLINE");
    cy.visit(homeUrl);
    cy.wait(`@CONSULTAS ONLINE`);

    cy.removeElementosDevHome();
    cy.CardsEFiltrosEspecialidades();

    cy.compareSnapshot(`page_CONSULTAS ONLINE`, {
      errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  it(`Acessa a página VACINAS no tablet e ${Cypress.env(
      "motivoteste"
  )} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["VACINAS"]}`;
    cy.intercept("GET", homeUrl).as("VACINAS");
    cy.visit(homeUrl);
    cy.wait(`@VACINAS`);

    cy.removeElementosDevHome();
    cy.CardsEFiltrosEspecialidades();

    cy.compareSnapshot(`page_VACINAS`, {
      errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  it(`Acessa a página ESPECIALIDADES MEDICAS no tablet e ${Cypress.env(
      "motivoteste"
  )} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["ESPECIALIDADES MEDICAS"]}`;
    cy.intercept("GET", homeUrl).as("ESPECIALIDADES MEDICAS");
    cy.visit(homeUrl);
    cy.wait(`@ESPECIALIDADES MEDICAS`);

    cy.removeElementosDevHome();
    cy.CardsEFiltrosEspecialidades();

    cy.compareSnapshot(`page_ESPECIALIDADES MEDICAS`, {
      blackout: [".vue-go-top"],
      errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  it(`Acessa a página EXAMES PARA EMPRESAS no tablet e ${Cypress.env(
      "motivoteste"
  )} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["EXAMES PARA EMPRESAS"]}`;
    cy.intercept("GET", homeUrl).as("ESPECIALIDADES MEDICAS");
    cy.visit(homeUrl);
    cy.wait(`@ESPECIALIDADES MEDICAS`);

    cy.removeElementosDevHome();
    cy.CardsEFiltrosEspecialidades();

    cy.compareSnapshot(`page_EXAMES PARA EMPRESAS`, {
      errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  it(`Acessa a página VACINAS PARA EMPRESAS no tablet e ${Cypress.env(
      "motivoteste"
  )} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["VACINAS PARA EMPRESAS"]}`;
    cy.intercept("GET", homeUrl).as("VACINAS PARA EMPRESAS");
    cy.visit(homeUrl);
    cy.wait(`@VACINAS PARA EMPRESAS`);

    cy.removeElementosDevHome();
    cy.CardsEFiltrosEspecialidades();

    cy.compareSnapshot(`page_VACINAS PARA EMPRESAS`, {
      errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  it(`Acessa a página LOCAÇÃO DE ESPAÇOS no tablet e ${Cypress.env(
    "motivoteste"
  )} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}${urls["LOCACAO DE ESPACOS"]}`;
    cy.intercept(homeUrl).as("LOCACAO DE ESPACOS");
    cy.visit(homeUrl);
    cy.wait(`@LOCACAO DE ESPACOS`);

    cy.removeElementosDevLocacaoDeEspacos();
    cy.CardsEFiltrosEspecialidades();
    cy.compareSnapshot(`page_LOCACAO DE ESPACOS`, {
      //blackout: ['.vue-go-top'],
      //blackout: ['.adopt-c-blcsFr'],
      capture: "fullPage",
      errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
    });
  });

  for (const [page, url] of Object.entries(urls)) {
    if (
        page === "HOME DEV" ||
        page === "CONSULTAS PRESENCIAIS" ||
        page === "EXAMES" ||
        page === "CONSULTAS ONLINE" ||
        page === "VACINAS" ||
        page === "ESPECIALIDADES MEDICAS" ||
        page === "EXAMES PARA EMPRESAS" ||
        page === "VACINAS PARA EMPRESAS" ||
        page === "LOCACAO DE ESPACOS" ||
        page === "RESULTADOS DE EXAMES"
    ) {
      continue;
    }

    it(`Acessa a página ${page} no tablet e ${Cypress.env(
      "motivoteste"
    )} imagens para validar layout`, () => {
      const fullUrl = `${env.baseUrl}${url}`;
      cy.intercept("GET", fullUrl).as(page);
      cy.visit(fullUrl);
      cy.wait(`@${page}`);

      cy.removeElementosTabletDev();

      cy.compareSnapshot(`page_${page}`, {
        blackout: ['.vue-go-top'],
        // blackout: ['.adopt-c-blcsFr'],
        capture: "fullPage",
        errorThreshold: Cypress.env("CYPRESSVR_ERROR_THRESHOLD"),
      });
    });
  }
});
