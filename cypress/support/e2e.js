
Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

import 'cypress-mochawesome-reporter/register'
import './commands'