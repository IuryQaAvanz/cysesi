const {
  addCompareSnapshotCommand,
} = require("cypress-visual-regression/dist/command");

addCompareSnapshotCommand({
  capture: "fullPage",
  fileName: (testName) => {
    return testName.split("describe(")[1].split(")")[0];
  },
});

Cypress.Commands.add("limparEstadoCypress", () => {
  cy.clearCookies();
  cy.clearLocalStorage();
  cy.reload(true);
  cy.clearAllSessionStorage();
});

Cypress.Commands.add("removeElementosHome", () => {
  cy.get(".main-container-wrapper .sticky-header").invoke(
    "css",
    "position",
    "relative"
  );

  cy.get("#adopt-controller-button", { timeout: 10000 }).then(($el) => {
    if ($el.length && $el.is(":visible")) {
      cy.wrap($el).invoke("remove");
    } else {
      cy.log(
        "Elemento #adopt-controller-button não encontrado ou não visível."
      );
    }
  });

  cy.scrollTo("bottom").then(() => {
    cy.get(".vue-go-top").invoke("remove");
  });

  cy.get(".adopt-c-blcsFr").invoke("remove");
  cy.get("a.bubble-chatbot").invoke("remove");
  cy.get("a.whatsapp-chat").invoke("remove");
  cy.get("#__talkjs_launcher").invoke("remove");
});

Cypress.Commands.add("removeElementos", () => {
  cy.get(".main-container-wrapper .sticky-header").invoke(
    "css",
    "position",
    "relative"
  );
  cy.scrollTo("bottom");

  cy.get("a.bubble-chatbot").invoke("remove");
  cy.get("a.whatsapp-chat").invoke("remove");
  cy.get("#__talkjs_launcher").invoke("remove");
});

Cypress.Commands.add("removeElementosDevHome", () => {
  cy.get(".main-container-wrapper .sticky-header").invoke(
    "css",
    "position",
    "relative"
  );

  cy.get(".phpdebugbar").invoke("remove");
  cy.get(".sesi-footer").scrollIntoView({ duration: 2000 }, { tolerance: 1 });
});

Cypress.Commands.add("removeElementosDev", () => {
  cy.get(".main-container-wrapper .sticky-header").invoke(
    "css",
    "position",
    "relative"
  );
  // cy.scrollTo("bottom", { duration: 2000 }).then(() => {
  //   cy.get(".vue-go-top").invoke("remove");
  // });

  cy.get(".phpdebugbar").invoke("remove");
  cy.get(".sesi-footer")
    .scrollIntoView({ duration: 2000 }, { tolerance: 1 })
    .then(() => {
      cy.get(".vue-go-top").invoke("remove");
    });
});

Cypress.Commands.add("removeElementosTabletDev", () => {
  cy.get(".main-container-wrapper .sticky-header").invoke(
    "css",
    "position",
    "relative"
  );
  cy.scrollTo("bottom", { ensureScrollable: false });
  cy.get(".phpdebugbar").invoke("remove");

  cy.scrollTo("bottom", { duration: 2000 }).then(() => {
    cy.get(".vue-go-top").invoke("remove");
  });
});

Cypress.Commands.add("removeElementosCelularHome", () => {
  cy.get(".main-container-wrapper .sticky-header").invoke(
    "css",
    "position",
    "relative"
  );

  cy.get("#adopt-controller-button", { timeout: 10000 }).then(($el) => {
    if ($el.length && $el.is(":visible")) {
      cy.wrap($el).invoke("remove");
    } else {
      cy.log(
        "Elemento #adopt-controller-button não encontrado ou não visível."
      );
    }
  });

  cy.get(".adopt-c-blcsFr").invoke("remove");
  cy.get("a.bubble-chatbot").invoke("remove");
  cy.get("a.whatsapp-chat").invoke("remove");
  cy.get("#__talkjs_launcher").invoke("remove");
});

Cypress.Commands.add("removeElementosMobileDev", () => {
  cy.get(".main-container-wrapper .sticky-header").invoke(
    "css",
    "position",
    "relative"
  );
  cy.get(".phpdebugbar").invoke("remove");
  cy.get(".sesi-footer").scrollIntoView({ tolerance: 1 });

  cy.get(".sesi-footer").scrollIntoView({ duration: 2000 }, { tolerance: 1 });
  //cy.scrollTo("bottom", { duration: 1000 });
});

Cypress.Commands.add("removeElementosDevLocacaoDeEspacos", () => {
  cy.get(".main-container-wrapper .sticky-header").invoke(
    "css",
    "position",
    "relative"
  );
  cy.get(".phpdebugbar").invoke("remove");
  cy.get(".sesi-footer").scrollIntoView({ duration: 2000 }, { tolerance: 1 });
});

Cypress.Commands.add("removeElementosDevDesktop", () => {
  cy.get(".main-container-wrapper .sticky-header").invoke(
    "css",
    "position",
    "relative"
  );

  cy.get(".phpdebugbar").invoke("remove");
  cy.get(".sesi-footer").scrollIntoView({ duration: 2000 }, { tolerance: 1 });
});

Cypress.Commands.add('CardsEFiltrosEspecialidades', () => {
  cy.get("#gridServicesCards", { timeout: 120000 }).should("exist").and("be.visible");
  cy.get(".filter-dropdowns", { timeout: 120000 }).should("exist").and("be.visible");
  // cy.get(
  //   ".services-cards.container.justify-content-center.justify-content-md-start",
  //   { timeout: 120000 }
  // ).should("exist").and("be.visible");
  cy.get(".dropdown .material-icons-round", { timeout: 120000 })
    .should("exist")
    .and("be.visible");
  cy.wait(500);
})